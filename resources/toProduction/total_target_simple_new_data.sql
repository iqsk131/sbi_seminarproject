DROP TABLE sport_target;
CREATE TABLE sport_target AS
SELECT 
	herold_new.gid,
	herold_new.firma AS company,
	herold_new.br AS industry,
	sport_type.type AS sport_type,
	SUM(r250_new.all_06+r250_new.all_07+r250_new.all_08) AS target,
	herold_new.geom
FROM herold_new, r250_new, sport_type
WHERE 
	herold_new.hic_id = 2901 AND
	r250_new.geom && st_buffer(herold_new.geom, 2000) AND
	sport_type.subtype = herold_new.br
GROUP BY 
	herold_new.gid,
	herold_new.firma,
	herold_new.br,
	sport_type.type,
	herold_new.geom
ORDER BY herold_new.gid;