--
-- PostgreSQL database dump
--

-- Dumped from database version 10.4
-- Dumped by pg_dump version 10.4

-- Started on 2018-11-30 17:08:40

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 241 (class 1259 OID 41075)
-- Name: sport_type; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.sport_type (
    type character varying(90),
    subtype character varying(90)
);


ALTER TABLE public.sport_type OWNER TO postgres;

--
-- TOC entry 4274 (class 0 OID 41075)
-- Dependencies: 241
-- Data for Name: sport_type; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.sport_type (type, subtype) FROM stdin;
Swimming	Hallenbäder
Swimming	Badeanstalten / Bäder
Tennis	Tennisanlagenbau
Tennis	Tennishallen u -plätze
Bowling	Kegel- u Bowlingbahnen
Bowling	Kegel- u Bowlingbahnen/Bau u Service
Golf	Golfplätze
Golf	Minigolfanlagen u -bedarf
Golf	Squash-Center
Fitness	Fitnesscenter
Fitness	Freizeit- u Sportbekleidung
Fitness	Kultur- u Sportveranstalter
Fitness	Sport- u Spielplatzanlagenbau
Fitness	Sportartikel u -geräte/Reparatur
Fitness	Sporthallen
Skate	Eislaufplätze
Gymnastic	Gymnastikinstitute
Motorsport	Motorsport
Horse Riding	Reitställe u -schulen
Ski	Ski/Vermietung
Summer Toboggan	Sommerrodelbahnen
Camping	Zelte/Vermietung
\.


-- Completed on 2018-11-30 17:08:44

--
-- PostgreSQL database dump complete
--

