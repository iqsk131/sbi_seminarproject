<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>MCCS-Hub</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
	<link href="css/main.css" rel="stylesheet">

    <!-- Custom styles -->
    <link href="css/sticky-footer-navbar.css" rel="stylesheet">
	<link rel="stylesheet" href="https://openlayers.org/en/v5.3.0/css/ol.css">
	<link href="https://cdnjs.cloudflare.com/ajax/libs/jquery-ui-bootstrap/0.5pre/css/custom-theme/jquery-ui-1.10.0.custom.css" rel="stylesheet"/>
	
  </head>

  <body onload="makeMap();" class="p-0 m-0">

   

    <!-- Begin page content -->
      <div class="container-fluid">
	  <div class="row">
	  <div class="col-12 p-0 m-0" >
		 <header>
			  <!-- Fixed navbar -->
			  <nav class="navbar navbar-expand-md navbar-dark bg-dark">
				<a class="navbar-brand" href="#">MCCS-Hub</a>
				<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
				  <span class="navbar-toggler-icon"></span>
				</button>
				<div class="collapse navbar-collapse" id="navbarCollapse">
				  <ul class="navbar-nav mr-auto">
					<li class="nav-item active">
					  <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
					</li>
					<li class="nav-item">
					  <a class="nav-link" href="#">About us</a>
					</li>
				  </ul>
				  
					<input class="form-control mr-2 mr-sm-2 w-25 autocomplete" type="text" placeholder="Search" aria-label="Search" id="filter" onkeyup="applyFilter($('#filter').val());" onclick="$('#filter').keydown()">
					 <input type="hidden" id="custId" name="custId" value="3487">
					<button class="btn btn-outline-success my-2 my-sm-0" onclick="updateMap($('#filter').val());">Search</button>
				  
				</div>
			  </nav>
		</header>
	  </div>
	  </div>
		  <div class="row">
			<div class="col-3" >
				<div class="h-25"></div>
				<legend>Legend</legend>
				<div class="row">
				<div class="col-1"></div>
				 <div class="col-5">
					<div class="w-75">
						  <div><img src="img/CircleLegend.PNG" alt="Distance"></div>
						  <div><img src="img/DistanceLegend.PNG" alt="Distance"></div>
					</div>
				 </div>
				 <div class="col-6">
					<div>
						  <div id="txt1">&nbsp;</div>
						  <div id="txt2">&nbsp;</div>
						  <div id="txt3">&nbsp;</div>
						  <div id="txt4">&nbsp;</div>
						  <div id="txt5">&nbsp;</div>
						  <div><p style="color: gray;">Inhabitants / Facility</p></div>
					</div>
				 </div>
				</div>
				
				<div class="h-25"></div>
				<div class="row">
					<div class="card w-100" style="visibility: hidden" id="card">
					  <div class="card-body">
						<h5 class="card-title" id="titel"></h5>
						<h6 class="card-subtitle mb-2 text-muted" id="bodyTitel"></h6>
						<p class="card-text" id="description"></p>
					  </div>
					</div>
				</div>
				<div class="h-25"></div>
			</div>
			<div class="col-9 p-0 m-0" >
				<div class="h-100" id="map">
				</div>
			</div>
		  </div>
			<div id="loading">Loading...</div>
		  <!-- End page content -->
		  <!-- Begin footer -->
		  <div class="row">
		  <div id="info" style="position: absolute; z-index: 10; font-color: black; background-color: #e8edf4; font-size: 12px;top: -10em; pointer-events: none;">&nbsp;</div>
			<footer class="footer">
			  <div class="container">
				<span class="text-muted">&copy; <b>MCCS-Hub</b>, 2018. Data credits to <b>HEROLD Business Data.</b> Map credits to <b>Openstreetmaps.</b></span>
			  </div>
			</footer>
		</div>
		<!-- End footer -->
	  </div>
	  
    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
	<script src="js/jquery.js"></script>
	<script src="js/jquery-ui.js"></script>
    <script src="js/bootstrap.min.js"></script>
	<script src="js/ol.js" type="text/javascript"></script>
	<script src="js/main.js" type="text/javascript"></script>
  </body>
</html>
