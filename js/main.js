var oMap = "";
var oBaseLayer = new ol.layer.Tile({
    source: new ol.source.TileWMS({
        url: 'http://localhost:8080/geoserver/wuseminar/wms',
        crossOrigin: "anonymous",
        params: {
            'layers': 'wuseminar:base_map',
            'sld_body': '<?xml version="1.0" encoding="UTF-8"?><StyledLayerDescriptor xmlns="http://www.opengis.net/sld" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:se="http://www.opengis.net/se" version="1.1.0" xsi:schemaLocation="http://www.opengis.net/sld http://schemas.opengis.net/sld/1.1.0/StyledLayerDescriptor.xsd" xmlns:ogc="http://www.opengis.net/ogc"><NamedLayer><se:Name>wuseminar:base_map</se:Name><UserStyle><se:Name>wuseminar:base_map</se:Name><se:FeatureTypeStyle><se:Rule><se:Name>Single symbol</se:Name><se:PolygonSymbolizer><se:Fill><se:SvgParameter name="fill">#000055</se:SvgParameter><se:SvgParameter name="fill-opacity">0.3</se:SvgParameter></se:Fill><se:Stroke><se:SvgParameter name="stroke">#232323</se:SvgParameter><se:SvgParameter name="stroke-width">1</se:SvgParameter><se:SvgParameter name="stroke-linejoin">bevel</se:SvgParameter></se:Stroke></se:PolygonSymbolizer></se:Rule></se:FeatureTypeStyle></UserStyle></NamedLayer></StyledLayerDescriptor>',
            'format': 'image/png'
        },
        transition: 0,
        srs: 'EPSG:3857'
    })
});
var oCompanySource = new ol.source.TileWMS({
    url: 'http://localhost:8080/geoserver/wuseminar/wms',
    crossOrigin: "anonymous",
    params: {
        'layers': 'wuseminar:sport_target',
        'format': 'image/png'
    },
    transition: 0,
    srs: 'EPSG:3857'
});
var oCompanyLayer = new ol.layer.Tile({
    source: oCompanySource
});
var oView = new ol.View({
    center: ol.proj.transform([13, 48], 'EPSG:4326', 'EPSG:3857'),
    zoom: 7
});
var iKeyUpTimeOut = 0;
var aAvailableTags = ["Bowling", "Fitness Facilities", "Golf and Minigolf", "Outdoor Sports", "Retail", "Squash and Tennis", "Water Sports", "Winter Sports"];

/*
*   @Name:          makeMap()
*   @Description:   Initialize map and layers, as well as handle all events
*/
function makeMap() {

    // Create map with 3 layers, OSM, base layer, and default target layer
    oMap = new ol.Map({
        target: 'map',
        layers: [
            new ol.layer.Tile({
                source: new ol.source.OSM()
            }),
            oBaseLayer,
            //oCompanyLayer
        ],
        view: oView
    });

    // Handle map click event - show company information when click
    oMap.on('click', function (evt) {
        // Get information from click location
        getInfo(evt, function(res) {
            if (res.bStatus) {
                // Display company information on sidebar
                $("#card").css('visibility', 'visible');
                $("#titel").html(res.sName);
                $("#bodyTitel").html("Type: " + res.sIndustry);
                $("#description").html("Target arround: " + res.iTarget + " Inhabitants");
            }
        });
    });

    // Handle map pointer move event - show company name when hover
    oMap.on('pointermove', function (evt) {
        // If still dragging, don't do anything
        if (evt.dragging) {
            $("#info").css("display", "none");
            return;
        }
        // Change the location of hover box
        var pixel = oMap.getEventPixel(evt.originalEvent);
        $("#info").css("left", (event.pageX - 50) + 'px');
        $("#info").css("top", (event.pageY - 20) + 'px');
        // Set hit event when pointer hit company layer
        var hit = oMap.forEachLayerAtPixel(pixel, function (layer, representation) {
            if (layer == oCompanyLayer) return true;
            return false;
        });
        if (hit) {
            // Get information from location
            getInfo(evt, function(res) {
                if (res.bStatus) {
                    // Display company name on hover box
                    $("#info").html(res.sName);
                    $("#info").css("display", "block");
                }
            });
            // Change cursour to pointer
            $("#map").css("cursor", "pointer");
        } else {
            // Hide hover box and change cursor to default
            $("#info").css("display", "none");
            $("#map").css("cursor", "");
        }
    });

    // Start update map with all sport type
    updateMap();

    // Add autocomplete function to search
    $(".autocomplete").autocomplete({
        source: aAvailableTags,
        minLength: 0,
        select: function (event, ui) {
            // Update map when select
            updateMap(ui.item.value);
        }
    });
}

/*
*   @Name:          updateMap()
*   @Parameter:     String filter
*   @Description:   Update map based on the filter parameter
*/
function updateMap(filter = "All") {
    if (filter == "") filter = "All";
    // Use php to build the SLD string
    $("#loading").css("display","");
    $.ajax({
        url: "map/request.php",
        data: {
            filter: filter
        },
        dataType: "json",
        success: function (res) {
            // Remove the old company layer
            oMap.removeLayer(oCompanyLayer);
            // Create a new company source with SLD
            oCompanySource = new ol.source.TileWMS({
                url: 'http://localhost:8080/geoserver/wuseminar/wms',
                crossOrigin: "anonymous",
                params: {
                    'layers': 'wuseminar:sport_target',
                    'sld_body': res.sSLD,
                    'format': 'image/png'
                },
                transition: 0,
                // Override the original tileLoadFunction from GET to POST method to prevent HTTP Error 414
                tileLoadFunction: function (image, src) {
                    var img = image.getImage();
                    if (typeof window.btoa === 'function') {
                        var xhr = new XMLHttpRequest();
                        var dataEntries = src.split("&");
                        var url;
                        var params = "";
                        for (var i = 0; i < dataEntries.length; i++) {
                            if (i === 0) {
                                url = dataEntries[i];
                            }
                            else {
                                params = params + "&" + dataEntries[i];
                            }
                        }
                        xhr.open('POST', url, true);
                        xhr.responseType = 'arraybuffer';
                        xhr.onload = function (e) {
                            if (this.status === 200) {
                                var uInt8Array = new Uint8Array(this.response);
                                var i = uInt8Array.length;
                                var binaryString = new Array(i);
                                while (i--) {
                                    binaryString[i] = String.fromCharCode(uInt8Array[i]);
                                }
                                var data = binaryString.join('');
                                var type = xhr.getResponseHeader('content-type');
                                if (type.indexOf('image') === 0) {
                                    $("#loading").css("display","none");
                                    img.src = 'data:' + type + ';base64,' + window.btoa(data);
                                }
                            }
                        };
                        xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
                        xhr.send(params);
                    } else {
                        img.src = src;
                    }
                },
                srs: 'EPSG:3857'
            });
            // Create a new company layer from new company source
            oCompanyLayer = new ol.layer.Tile({
                source: oCompanySource
            });
            // Add a new company layer to map
            oMap.addLayer(oCompanyLayer);
            // Set Legend
            $("#txt1").html("Less than " + res.aBreakers[1]);
            $("#txt2").html(res.aBreakers[1] + " to " + res.aBreakers[2]);
            $("#txt3").html(res.aBreakers[2] + " to " + res.aBreakers[3]);
            $("#txt4").html(res.aBreakers[3] + " to " + res.aBreakers[4]);
            $("#txt5").html("More than " + res.aBreakers[4]);
        }
    });
}

/*
*   @Name:          applyFilter()
*   @Parameter:     String filter
*   @Description:   Delay updateMap() for 500ms
*/
function applyFilter(filter) {
    // If this function is called again before 500ms, clear the timeout
    clearTimeout(iKeyUpTimeOut);
    iKeyUpTimeOut = setTimeout(function () {
        updateMap(filter);
    }, 500);
}

/*
*   @Name:          getInfo()
*   @Parameter:     Object evt, Function callback
*   @Description:   Get Information from location in OpenLayer
*/
function getInfo(evt, callback) {
    var viewResolution = /** @type {number} */ (oView.getResolution());
    // Build request url
    var url = oCompanySource.getGetFeatureInfoUrl(evt.coordinate, viewResolution, 'EPSG:3857', { 'INFO_FORMAT': 'text/html' });
    // Specify layer to be query in GeoServer
    url = url + "&query_layers=wuseminar%3Asport_target"; 
    // Remove sld_body attribute from URL to prevent HTTP Error 414
    url = url.replace(/sld_body=[^&]*&/, ''); 
    // Get filter value
    filter = $('#filter').val();
    if (filter == "") filter = "All";
    if (url) {
        // Since the getFeatureInfo request return with HTML page, use php to extract the information
        $.ajax({
            url: "map/getinfo.php",
            data: {
                request: url,
                filter: filter
            },
            method: "POST",
            dataType: "json",
            success: function (res) {
                // Callback the function
                callback(res);
            }
        });
    }
}
