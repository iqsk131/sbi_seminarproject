<?php
    $response = new stdClass();
    if(isset($_POST["request"])){
        $request_url = $_POST["request"];
        // Load HTML source code from request url
        $content = file_get_contents($request_url);
        if(strpos($content, "industry")){
            // Split the source code and get information
            $lcontent = preg_split("/<.?td>/", $content);
            $response->sName = $lcontent[5];
            $response->sIndustry = $lcontent[7];
            $response->iTarget = floor($lcontent[11]);
            // If the select is out of filter, don't show the data
            if($_POST["filter"] != "All" && $lcontent[9] != strtolower($_POST["filter"])) 
                $response->bStatus = false;
            else
                $response->bStatus = true;
        }
        else{
            $response->bStatus = false;
        }
        $response->sURL = $request_url;
        $response->sContent = $content;
    }
    echo json_encode($response);
?>