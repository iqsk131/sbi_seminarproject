<?php
    include("../db/db.php");
    if(isset($_GET["filter"])) $filter = $_GET["filter"];
    $response = new stdClass();

    // Build the query only selected filter
    $query = "SELECT * FROM sport_target";
    if($filter != "All")
        $query = $query . " WHERE sport_type = '" . strtolower($filter) . "'";
    else
        $filter = ".";

    // Do the query
    $query_result = pg_query($dbconn, $query);

    // Create a list of target value from query result
    $target_list = array();
    while ($row = pg_fetch_row($query_result)) {
        array_push($target_list,$row[4]);
    }
    // Sort the list by target
    sort($target_list);

    // Break by equal count
    //*
    $last_index = sizeof($target_list) - 1;
    $breaker = array();
    for($i=0;$i<=5;$i++){
        array_push($breaker,ceil($target_list[$i*$last_index/5]));
    }
    $breaker[0]=floor($target_list[0]);
    $breaker[5]=ceil($target_list[$last_index]);
    //*/

    // Break by equal interval
    /*
    $last_index = sizeof($target_list) - 1;
    $interval = ($target_list[$last_index] - $target_list[0])/5.0;
    $breaker = array();
    for($i=0;$i<=5;$i++){
        array_push($breaker,floor($target_list[0] + $interval*$i));
    }
    $breaker[0]=floor($target_list[0]);
    $breaker[5]=ceil($target_list[$last_index]);
    //*/

    // Load Template
    $template_sld = file_get_contents("../resources/template.sld");
    // Replace filter in template
    $template_sld = str_replace("#FILTER", $filter, $template_sld);
    // Replace breaker in template
    for($i=0;$i<=5;$i++){
        $template_sld = str_replace("#BREAKER".$i, $breaker[$i], $template_sld);
    }
    // Copy to json
    $response->sSLD = $template_sld;
    $response->aBreakers = $breaker;
    echo json_encode($response);
?>